// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const serialport = require('serialport');
const Tone = require('tone');
const Mousetrap = require('mousetrap');

const Com = require('./js/com');
const Controller = require("./js/midi-controller");

//const arduino = new arSerial()

$(document).ready(function(){
	main()
});

let control;

function str_pad_left(string,pad,length) {
    return (new Array(length+1).join(pad)+string).slice(-length);
}

let playing = false;
function togglePlay(){
	try{
		control.toggle();
	}catch(e){
		return;
	}
	if(playing){
		$play.removeClass("fa-pause").addClass("fa-play");
	}else{
		$play.removeClass("fa-play").addClass("fa-pause");
	}
	playing = !playing;
}

function createKeyboardEvents(){
	function only(func){ // This function makes the default event not have any effect
		return () => {
			try{
				func()
			}catch(e){
				return false;
			}
			return false;
		}
	}

	Mousetrap.bind("space", only(() => togglePlay()));
	Mousetrap.bind("left", only(() => control.jump(-0.75)));
	Mousetrap.bind("right", only(() => control.jump(0.75)));
	Mousetrap.bind("up", only(() => control.setSpeed(control.speed+0.25)));
	Mousetrap.bind("down", only(() => control.setSpeed(control.speed-0.25)));
}

function setupSliderAndTime(){
	$slider = $("#song_position");
	$time = $("#time");
	let selecting = false;
	$slider.val(0);
	control.setPosChangeCallback((pos) => {
		if(!selecting) $slider.val((pos/control.duration)*$slider.prop("max"));
		const minutes = Math.floor(pos / 60);
		const seconds = Math.floor(pos) - minutes * 60;
		$time.text(str_pad_left(minutes,'0',2)+':'+str_pad_left(seconds,'0',2));
	})
	$slider.mousedown(e => {
		selecting = true;
	})
	$slider.mouseup(e => {
		selecting = false;
	})
	$slider.on("change", async e => {
		const t = $(e.target);
		const prop = t.val()/t.prop("max");
		const pos = control.duration * prop;
		control.setPos(pos);
	})
}

function main(){
	createKeyboardEvents();
	$play = $("#play-button")
	$play.click(e => {
		togglePlay();
	})

	$loop = $("#loop");
	$loop.click(e => {
		if(control.looping.active){
			$loop.text("Start Loop");
			control.stopLoop();
		}else{
			$loop.text("Stop Loop")
			control.startLoop({section: $("#section-in").val()});
		}
	})

	$learn = $("#learn-button");
	$learn.click(e => {
		if($learn.hasClass("learning")){
			$learn.text("Start Learning");
			$learn.removeClass("learning");
			control.stopLearning();
		}else{
			$learn.text("Stop Learning");
			$learn.addClass("learning");
			control.arduinoLearn();
		}
	})

	let com;
	$buzz = $("#buzz");
	$buzz.click(async e => {
		if(!com){
			com = new Com();
			try{
				await com.setup();
				console.log("Opened arduino serial port");
			}catch(e){
				console.error("Could not open: ",e);
			}
		}
	})

	$buzzIn = $("#buzz-nums");
	$buzzIn.change(() => {
		const fingers = $buzzIn.val().split("").map(f => parseInt(f));
		console.log("Activating:",fingers);
		com.sendNotes(fingers.map(f => ({hand: "left", finger: f})));
	})

	$piano = $("#piano");
	$piano[0].addEventListener('wheel',function(event){
		const dist = -1*event.deltaY;
	    control.jump(dist/100);
	    event.preventDefault();
	    return false; 
	}, false);

	window.addEventListener("resize", () => {
		control.restartRenderer();
	})

	$("#file_in").on("change", (e) => {
		const file = $("#file_in").prop("files")[0]
		const fr = new FileReader();
		if(control) control.cleanup();
		fr.onload = async () => {
			control = await Controller.setup(fr.result);
			setupSliderAndTime();
			$piano.select();
		};
		fr.readAsArrayBuffer(file)
	})
	process.on('exit', function() {
	    control.cleanup();
	});
	window.onbeforeunload = e => {
	    control.cleanup();
	};
}