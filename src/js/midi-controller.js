// This will contain all of the high level stuff like timings while sending render and play instructions to midi-players and midi-renderers.

// TODO: If the speed is changed while a note is being played. That pitch never gets released. 
// TODO: Make it so that you can get it to loop sections.
const Tone = require("tone");
const Midi = require("@tonejs/midi").Midi;
const path = require("path");
const fs = require("fs");

const remote = require('electron').remote;
const app = remote.app;

const Player = require("./midi-player");
const Renderer = require("./midi-renderer");
const Com = require("./com");

class Controller{
	constructor(file){
		this.midi = new Midi(file); //Loads the data from the opened file
		this.pos = 0; //Stores the current position of the song
		this.runner = 0; //Stores the id of the interval that is running the song
		this.speed = 1; //Stores the speed value that is being used to calculate the position
		this.playedNotes = []; //Stores a list of all notes that have already been played when the position was set
		this.duration = this.midi["duration"];
		this.sections = [{"start": 0, "end": this.duration}];
		this.canvasId = "#piano";
		this.saveFile = `${this.midi.name}-${this.midi.durationTicks}`.replace(/[^a-z0-9]/gi, '_').toLowerCase()+".json";
		this.looping = {active: false, start: 0, end: this.duration};
		this.minLength = 0.3; //Defines the shortest amount of time that a note should be vibrated for
		this.setPosChangeCallback();
		this.setFigeringAddedCallback();
		this.setSectionAddedCallback();
		this.setSectionRemovedCallback();
	}

	// Startup and Construction
	static async setup(file){
		const control = new Controller(file);

		console.log(control.saveFile);

		control.tracks = control.midi.tracks.filter(track => track.notes.length > 0);
		control.tracks.forEach((track, trackI) => track.notes.forEach((note, noteI) => {
			note["hand"] = "";
			note["finger"] = -1;
			note["index"] = noteI;
			note["track"] = trackI;
			note["autofilled"] = false;
		}))

		try{
			await control.load();
		}catch(e){
			console.log("No Save Found");
		}

		control.player = await Player.setup();
		control.renderer = await Renderer.setup({canvasId: control.canvasId, onFingerAdd: control.onFingerAdd, onSectionAdd: control.onSectionAdd, onSectionRm: control.onSectionRm, control: control});
		try{
			control.com = new Com();
			await control.com.setup();
		}catch(e){
			console.log("Failed to get com:",e)
			control.com = false;
		}

		console.log("Prepping render")
		control.prepRenderer();
		control.prepNotes();

		control.render();
		return control;
	}

	prepNotes(speed=1, position=0){
		const dialation = 1/speed;
		this.player.clear();
		this.player.syncAnimation(() => {
			this.onPosChange(this.getPos());
			//console.log("Animation playing",Tone.now());
		});
		this.setPos(position);
		this.player.resetNotes();
		this.tracks.forEach((track, trackI) => {
			track.notes.forEach(note => {
				this.player.addNote({
					pitch: note.pitch,
					octave: note.octave,
					duration: note.duration*dialation,
					time: note.time*dialation,
					velocity: note.velocity,
					track: note.track,
					index: note.index
				}, this.minLength)
			})
		})
		this.onPosChange(this.getPos());
	}

	prepRenderer(){
		const notes = [];
		this.tracks.forEach((track, trackI) => track.notes.forEach(note => {
			notes.push({
				pitch: note["pitch"],
				octave: note["octave"],
				time: note["time"],
				duration: note["duration"],
				track: note["track"],
				index: note["index"],
				hand: note["hand"],
				finger: note["finger"]
			})
		}));
		this.renderer.setNotes(notes);
		this.renderer.setSections(this.sections);
	}

	// Events and Callbacks
	setPosChangeCallback(func){
		this.onPosChange = (pos) => {
			if(func){
				func(pos);
			}
			if(this.looping.active && (pos<this.looping.start || pos>this.looping.end)) this.setPos(this.looping.start);
			if(pos > this.duration) this.setPos(this.duration);
			this.render(pos);
		};
	}

	setFigeringAddedCallback(func){
		this.onFingerAdd = (trackNum, noteIndex, hand, finger) => {
			const note = this.tracks[trackNum]["notes"].find(note => note["index"] == noteIndex);
			note["hand"] = hand;
			note["finger"] = finger;
			if(func){
				func(note);
			}
			this.save();
			this.prepRenderer();
			this.render();
		}
	}

	setSectionAddedCallback(func){
		this.onSectionAdd = (pos) => {
			if(func){
				func(section);
			}
			const i = this.sections.findIndex(elem => elem["start"]<pos && elem["end"]>pos);
			this.sections.splice(i, 1,
				{"start": this.sections[i]["start"], "end": pos},
				{"start": pos, "end": this.sections[i]["end"]}
			);
			this.save();
			this.prepRenderer();
			this.render();
		}
	}

	setSectionRemovedCallback(func){
		this.onSectionRm = (index) => {
			if(func){
				func(section);
			}
			if(index === 0) return;
			this.sections[index-1]["end"] = this.sections[index]["end"];
			this.sections.splice(index, 1);
			this.save();
			this.prepRenderer();
			this.render();
		}
	}

	getPos(){
		return this.player.getPos()*this.speed;
	}

	setPos(pos){
		const newPos = pos/this.speed;
		this.player.setPos(newPos);
		this.onPosChange(pos);
	}

	// Main Control
	render(pos){
		if(!pos) pos = this.getPos();
		this.renderer.render(pos);
	}

	play(params={}){
		let {speed = this.speed, position} = params;
		if(speed !== this.speed && speed > 0 && speed < 5){
			this.setSpeed(speed);
		}
		if(position){
			this.setPos(position);
		}
		if(this.com) this.com.resume();
		this.player.play();
	}

	pause(){
		if(this.com) this.com.pause();
		clearInterval(this.runner);
		this.player.pause();
	}

	toggle(){
		if(this.player.paused()){
			this.play();
		}else{
			this.pause();
		}
	}

	startLoop(params){
		const {section, sectionTime, start, end} = params;
		console.log(section, sectionTime, start, end, section && !isNaN(section), section && !isNaN(section));
		const loopTimes = (start, end) => {
			this.looping = {active: true, start: start, end: end};
			console.log("Looping:", this.looping, this.sections);
		}

		if(start !== undefined && end !== undefined && end > start){
			// Then we will just loop those times.
			loopTimes(start, end);
			return true;
		}else if(section !== undefined && !isNaN(section)){
			// Then we will prepare to loop that section
			if(section < this.sections.length) loopTimes(this.sections[section].start, this.sections[section].end);
			return true;
		}else if(sectionTime !== undefined && !isNaN(section)){
			// Then we will find the section then prepare to loop it
			const sections = this.sections.filter(section => section.start < sectionTime && section.end > sectionTime);
			if(sections.length > 0) loopTimes(sections[0].start, sections[0].end);
			return true;
		}else{
			// Then we are looping the entire song
			loopTimes(0, this.duration);
			return true;
		}
		return false;
	}

	stopLoop(){
		this.looping = false;
	}

	restart(){
		this.pause();
		if(this.com) this.com.clear();
		this.prepNotes(this.speed, 0);
		this.render();
	}

	jump(deltaT){
		if(this.com) this.com.clear();
		let newTime = this.getPos()+deltaT;
		if(newTime < 0) newTime = 0;
		if(newTime > this.duration) newTime = this.duration;
		this.setPos(newTime);
	}

	// Interaction
	fill(tracks){
		if(!tracks) tracks = this.tracks;
		const noteMap = {};
		tracks.forEach(track => track.notes.forEach(note => {
			if(note.hand.length < 1 || note.finger === -1){
				if(note.name in noteMap) ({hand: note.hand, finger: note.finger, autofilled: note.autofilled} = noteMap[note.name]);
			}else{
				noteMap[note.name] = {hand: note.hand, finger: note.finger, autofilled: true}
			}
		}))
		this.prepRenderer();
		this.render();
	}

	getFingerings(fill=true){
		this.fill();
		const notes = [];
		this.tracks.forEach((track, trackI) => track.notes.forEach(note => {
			if(note.finger !== -1 && note.hand.length > 0){
				notes.push({
					"track": note["track"],
					"index": note["index"],
					"hand": note["hand"],
					"finger": note["finger"]
				})
			}
		}))
		return notes;
	}

	setSpeed(speed=1){
		const paused = this.player.paused();
		const newPos = this.getPos();
		this.speed = speed;
		if(this.com) this.com.clear();
		this.prepNotes(this.speed, newPos);
		if(!paused){
			this.play();
		}
	}

	async restartRenderer(){
		this.renderer.cleanup();
		this.renderer = await Renderer.setup({canvasId: this.canvasId, onFingerAdd: this.onFingerAdd, onSectionAdd: this.onSectionAdd, onSectionRm: this.onSectionRm, control: this});
		this.prepRenderer();
		this.render();
	}

	save(dir){
		return new Promise(async(resolve, reject) => {
			if(!dir) dir = path.join(app.getPath("appData"), "haptic-learning", "saves");
			try{
				await fs.promises.mkdir(dir, { recursive: true });
			}catch(e){}
			const fileLoc = path.join(dir, this.saveFile);
			const save = {notes: [], sections: this.sections};
			this.tracks.forEach(track => track.notes.forEach(note => {
				if(note["finger"] !== -1 && note["hand"].length > 0 && !note["autofilled"]) save.notes.push({track: note["track"], index: note["index"], finger: note["finger"], hand: note["hand"], autofilled: note["autofilled"]});
			}));
			fs.writeFile(fileLoc, JSON.stringify(save), (e) => {
			    if(e) {
			        return reject(e);
			    }
			    resolve();
			}); 
		})
	}

	load(dir){
		return new Promise((resolve, reject) => {
			if(!dir) dir = path.join(app.getPath("appData"), "haptic-learning", "saves");
			const fileLoc = path.join(dir, this.saveFile);
			try{
				const save = JSON.parse(fs.readFileSync(fileLoc));
				save.notes.forEach(note => {
					const oNote = this.tracks[note["track"]].notes[note["index"]];
					if(note["finger"] !== -1 && note["hand"].length > 0) ({finger: oNote.finger, hand: oNote.hand, autofilled: oNote.autofilled} = note);
				})
				this.sections = save.sections;
				resolve();
			}catch(e){
				reject(e);
			}
		})
	}

	async arduinoLearn(){
		if(!this.com){
			try{
				this.com = new Com();
				await this.com.setup();
			}catch(e){
				console.error("Failed to open arduino:", e);
				return false;
			}
		}else{
			this.com.clear();
		}
		this.fill();
		this.player.setOnNotePlayed((track, index) => {
			this.com.addNote(this.tracks[track].notes[index]);
		})
		this.player.setOnNoteEnd((track, index) => {
			this.com.removeNote(this.tracks[track].notes[index])
		})
	}

	stopLearning(){
		this.player.setOnNotePlayed();
		this.player.setOnNoteEnd();
		this.com.sendNotes([]);
	}

	// Clean up
	cleanup(){
		clearInterval(this.runner);
		console.log("Cleaning up:",this.com)
		if(this.com){
			this.com.cleanup();
		}
	}
}

module.exports = Controller;