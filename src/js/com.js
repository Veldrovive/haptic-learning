const SerialPort = require("serialport");

class Com{
	constructor(params={}){
		({
			ports: this.ports = [401, 101, 601],
			pad: this.pad = 20,
		} = params)
		this.currNotes = [];
		this.paused = false;
		this.arduinos = [];
	}

	async setup(portNum=0){
		return new Promise((resolve, reject) => {
			SerialPort.list((err, ports) => {
				ports = ports.filter(port => port.manufacturer !== undefined && port.manufacturer.includes("Arduino"));
				if(ports.length === 0) return reject("Arduino not connected");

				let count = 0;
				const finish = () => {
					count++;
					if(count >= ports.length){
						resolve();
					}
				}

				ports.forEach(port => {
					let arduino;
					arduino = new SerialPort(port["comName"], {baudRate: 9600}, (e) => {
						if(e){
							console.error("Failed to open serialPort:",port,e);
						}else{
							this.arduinos.push(arduino);
						}
						finish();
					})
				})
			})
		})
	}

	addNote(note){
		this.currNotes.push(note)
		this.sendCurrent();
	}

	removeNote(note){
		this.currNotes = this.currNotes.filter(n => n["track"] !== note["track"] || n["index"] !== note["index"]);
		this.sendCurrent();
	}

	clear(){
		this.currNotes = [];
	}

	pause(){
		this.sendNotes([]);
		this.paused = true;
	}

	resume(){
		console.log("Resuming");
		this.paused = false;
		this.sendCurrent();
	}

	sendCurrent(){
		const notes = [];
		this.currNotes.forEach(note => {
			if(note["finger"] !== -1 && note["hand"].length > 0){
				notes.push({hand: note["hand"], finger: note["finger"]});
			}
		})
		this.sendNotes(notes);
	}

	sendNotes(notes){
		// Notes: [{hand: "left"/"right", finger: 1-5},...]
		return new Promise((resolve, reject) => {
			const hands = {"left":[0,0,0,0,0], "right":[0,0,0,0,0]};
			if(this.paused) return resolve();
			notes.forEach(note => {
				hands[note["hand"]][note["finger"]-1] = 1;
			})
			const encHands = ["R".charCodeAt(0)].concat(hands["left"].reverse()).concat(hands["right"]);

			let count = 0;
			const finish = () => {
				count++;
				if(count >= this.arduinos.length){
					resolve();
				}
			}

			this.arduinos.forEach(arduino => {
				arduino.write(encHands, e => {
					if(e){
						console.log("Failed to write to:",arduino);
					}
					finish();
				})
			})
		})
	}

	cleanup(){
		if(this.arduino){
			this.sendNotes()
			this.arduino.close();
		}
	}
}

module.exports = Com;