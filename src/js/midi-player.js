// This will play piano notes when instructed by the midi-controller.
const path = require("path");

class Player{
	constructor(params={}){
		this.partOffSet = 0.1;
		({
			onNotePlayed: this.onNotePlayed = () => {},
			onNoteEnd: this.onNoteEnd = () => {},
			animFreq: this.animFreq = 0.03
		} = params)
		this.setOnNotePlayed();
		this.setOnNoteEnd();
		this.baseBpm = Tone.Transport.bpm.value;
	}

	static async setup(){
		const player = new Player();
		try{
			player.sampler = await player.loadSamples();
			player.sampler.sync();
			// player.addPart();
		}catch(e){
			console.error("Player Setup Error:",e);
		}
		return player;
	}

	loadSamples(){
		return new Promise((resolve, reject) => {
			const samples = {};
			["A", "B", "C", "D", "E", "F", "G"].forEach(pitch => {
				[...Array(7).keys()].forEach(octave => {
					samples[`${pitch}${octave}`] = path.join("./piano", `${pitch}${octave}.mp3`);
					if(["A", "C", "D", "F", "G"].includes(pitch)){
						samples[`${pitch}#${octave}`] = path.join("./piano", `${pitch}s${octave}.mp3`);
					}
				})
			})
			try{
				let sampler;
				sampler = new Tone.Sampler(samples, function(e){
					sampler.toMaster();
					resolve(sampler)
				})
			}catch(e){
				reject(e)
			}
		})
	}

	// Events
	setOnNotePlayed(func){
		this.onNotePlayed = (track, index) => {
			if(func){
				func(track, index);
			}
		}
	}

	setOnNoteEnd(func){
		this.onNoteEnd = (track, index) => {
			if(func){
				func(track, index);
			}
		}
	}

	// Functionality
	addNote(note, minTime){
		let {pitch, octave, duration, time, velocity, track, index} = note;
		this.sampler.triggerAttackRelease(
			pitch+octave,
			duration,
			time,
			velocity
		)
		Tone.Transport.schedule(() => this.onNotePlayed(track, index), time);
		if(minTime && duration < minTime) duration = minTime;
		Tone.Transport.schedule(() => this.onNoteEnd(track, index), time+duration);
	}

	resetNotes(){
		this.sampler.releaseAll();
	}

	clear(){
		Tone.Transport.stop();
		Tone.Transport.cancel();
	}

	getPos(){
		return Tone.Transport.getSecondsAtTime();
	}

	setPos(seconds){
		Tone.Transport.seconds = seconds;
		this.sampler.releaseAll();
	}

	play(){
		Tone.Transport.start();
		this.part.start(0);
	}

	syncAnimation(func){
		Tone.Transport.scheduleRepeat(func, this.animFreq);
	}

	paused(){
		return Tone.Transport.state === "paused" || Tone.Transport.state === "stopped";
	}

	pause(){
		Tone.Transport.pause();
	}

	// Clean Up
	cleanup(){
		this.clear();
	}
}

module.exports = Player;