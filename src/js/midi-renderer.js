// This will have control over a canvas and render views in accordance with what is passed in by the midi-controller.

class NoteRenderer{
	constructor(ctx){
		this.ctx = ctx;
	}

	getNoteNum(note, minNote){
		let min;
		if(!minNote){
			min = 0;
		}else if(typeof(minNote) === "number"){
			min = minNote;
		}else{
			min = Math.floor(this.noteMap(minNote));
		}
		return this.noteMap(note)+min;
	}

	numToNote(num, minNote){
		let min;
		if(!minNote){
			min = 0;
		}else if(typeof(minNote) === "number"){
			min = minNote;
		}else{
			min = Math.floor(this.noteMap(minNote));
		}
		const octave = Math.floor((min+num)/7)
		const curr = (min+num+2)%7;
		let note = String.fromCharCode(curr+65)
		if(Math.floor(curr) !== curr){
			note += "#";
		}
		return {pitch: note, octave: octave};
	}

	noteMap(note, includeOctaves=true){
		let num = (note.pitch.charCodeAt()-4)%7;
		if(note.pitch.length > 1){
			num += 0.5;
		}
		if(includeOctaves){
			num += 7*note.octave;
		}
		return num;
	}

	roundRect(x, y, width, height, radius=5, fill=true, stroke=false) {
		this.ctx.beginPath();
		this.ctx.moveTo(x + radius, y);
		this.ctx.lineTo(x + width - radius, y);
		this.ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
		this.ctx.lineTo(x + width, y + height - radius);
		this.ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
		this.ctx.lineTo(x + radius, y + height);
		this.ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
		this.ctx.lineTo(x, y + radius);
		this.ctx.quadraticCurveTo(x, y, x + radius, y);
		this.ctx.closePath();
		if (stroke) {
			this.ctx.stroke();
		}
		if (fill) {
			this.ctx.fill();
		}
	}
}

class KeyboardRenderer extends NoteRenderer{
	constructor(params){
		let keyprop;
		super();
		({
			keyprop,
			ctx: this.ctx,
			minNote: this.minNote = 0,
			maxNote: this.maxNote = 87,
			range: this.range = 88
		} = params)
		this.height = this.ctx.canvas.height;
		this.width = this.ctx.canvas.width;
		this.keyHeight = this.height*keyprop;
		this.keyWidth = this.width/this.range;

		this.bKeyGrad = this.ctx.createLinearGradient(0, this.height-this.keyHeight*1.5, 0, this.height+this.keyHeight*0.5);
		this.bKeyGrad.addColorStop(1, "black");
		this.bKeyGrad.addColorStop(0, "white");
		this.wKeyGrad = this.ctx.createLinearGradient(0, this.height-this.keyHeight*1.5, 0, this.height+this.keyHeight*0.5);
		this.wKeyGrad.addColorStop(1, "black");
		this.wKeyGrad.addColorStop(0, "white");

		this.setup();
	}

	setup(){
		this.noteLocs = [];
		for(let i=0; i<this.range; i++){
			const note = this.numToNote(i, this.minNote);
			this.noteLocs.push({
				pitch: note.pitch,
				octave: note.octave,
				num: this.getNoteNum(note),
				min: i*this.keyWidth,
				max: (i+1)*this.keyWidth
			})
			if(["C", "D", "F", "G", "A"].includes(note.pitch)){
				this.noteLocs.push({
					pitch: note.pitch+"#",
					octave: note.octave,
					num: this.noteMap(note)+0.5,
					min: (i+0.75)*this.keyWidth,
					max: (i+1.25)*this.keyWidth
				})
			}
		}
	}

	getNoteLocations(){
		return this.noteLocs;
	}

	render(notes){
		const indexesHeld = notes.filter(note => note["playing"] === true).map(note => note["num"]-this.minNote);
		for(let i=0; i<this.range; i++){
			this.ctx.fillStyle = indexesHeld.includes(i) ? this.wKeyGrad : "#FFFFFF";
			this.ctx.strokeStyle = "#000000";
			this.ctx.fillRect(i*this.keyWidth, this.height-this.keyHeight, this.keyWidth, this.keyHeight);
			this.ctx.strokeRect(i*this.keyWidth, this.height-this.keyHeight, this.keyWidth, this.keyHeight);
		} 
		for(let i=0; i<this.range; i++){
			const note = this.numToNote(i, this.minNote);
			if(["C", "D", "F", "G", "A"].includes(note.pitch)){
				this.ctx.fillStyle = indexesHeld.includes(i+0.5) ? this.bKeyGrad : "#000000";
				this.roundRect((i+0.75)*this.keyWidth, this.height-this.keyHeight, this.keyWidth/2, this.keyHeight*0.7, 2)
			}
		}
	}
}

class NoteboardRenderer extends NoteRenderer{
	constructor(params){
		let keyprop;
		super();
		({
			keyprop,
			ctx: this.ctx,
			noteLocations: this.noteLocs,
			yTime: this.yTime = 4
		} = params)
		this.boardHeight = (1-keyprop)*this.ctx.canvas.height;
	}

	render(notes=[], sections=[], regPad=8, sharpPad=3){
		// notes is a list of notes, the time until they are played, and the length of the note (Maybe also the track the note is on)
		// Format {pitch: "A", octave: 1, duration: 0.13, outTime: 2.0, hand: "left", finger: 1, color: "#28b54b", orig: A note}
		// sections is a list of times until a new section starts
		const self = this;
		const currNotes = [];
		const noteLocations = [];
		const sectionLocations = [];
		notes.forEach(note => {
			const loc = self.noteLocs.find(elem => elem.num === note["num"]);
			if(!loc) return;
			const height = (note.duration/self.yTime)*self.boardHeight;
			const yLoc = (1-((note.outTime+note.duration)/self.yTime))*self.boardHeight;
			this.ctx.fillStyle = note["color"];
			let pad;
			if(note["num"] % 1 === 0.5){
				pad = sharpPad;
			}else{
				pad = regPad;
			}
			this.roundRect(loc.min+pad, yLoc, loc.max-loc.min-pad*2, height, 8);
			if(note.finger !== -1){
				this.ctx.font = "20px Arial";
				this.ctx.textAlign = "center";
				this.ctx.textBaseline = "middle";
				this.ctx.fillStyle = "black";
				this.ctx.fillText(note.finger, (loc.min+loc.max)/2, yLoc+height/2);
			}
			noteLocations.push({note: {track: note["track"], index: note["index"], num: note["num"]}, xMin: loc.min+pad, yMin: yLoc, xMax: loc.max-pad, yMax: yLoc+height});
			if(note.outTime < 0){
				currNotes.push(note);
			}
		})
		sections.forEach(section => {
			if(section["outTime"] > self.yTime || section["outTime"] < 0) return;
			const height = 8;
			const width = 20;
			const yLoc = (1-(section["outTime"]/self.yTime))*self.boardHeight;
			this.ctx.strokeStyle = "#cc0000";
			this.ctx.beginPath();
			this.ctx.moveTo(0, yLoc+height/2);
			this.ctx.lineTo(this.ctx.canvas.width, yLoc+height/2);
			this.ctx.stroke();
			this.ctx.fillStyle = "#FFFFFF";
			this.roundRect(-10, yLoc, 10+width, height, 5);
			sectionLocations.push({index: section["index"], xMin: 0, yMin: yLoc, xMax: width, yMax: yLoc+5});
		})
		return {noteLocations: noteLocations, sectionLocations: sectionLocations};
	}
}

class Renderer extends NoteRenderer{
	constructor(params){
		super();
		let canvasId, ctx;
		({ 
			canvasId,
			ctx,
			width: this.width = window.innerWidth,
			height: this.height = window.innerWidth*0.4,
			yTime: this.yTime = 4,
			onFingerAdd: this.onFingerAdd = () => {},
			onSectionAdd: this.onSectionAdd = () => {},
			onSectionRm: this.onSectionRm = () => {},
			control: this.control
		} = params);
		if(canvasId) ctx = $(canvasId)[0].getContext('2d');
		super.ctx = ctx;
		this.canvas = ctx.canvas;
		this.canvas.width = this.width;
		this.canvas.height = this.height;
		this.createClickEvents();

		this.lastHand = "left";
		this.prevNoteMap = {};

		this.colors = ["#1f6bcf", "#cf831f"];
		this.handColors = {"left": "#28b54b", "right": "#cf1f31" };
	}

	static async setup(params){
		const renderer = new Renderer(params);
	
		return renderer;
	}

	setNotes(notes){
		notes.forEach(note => {
			note["num"] = this.noteMap(note);
			note["color"] = this.colors[note["track"]%this.colors.length];
		});
		this.notes = notes;
		({ min: this.min, max: this.max, range: this.range } = this.getExtremes(notes));
		this.keyboard = new KeyboardRenderer({keyprop: 1/4, ctx: this.ctx, minNote: Math.floor(this.min["num"]), range: this.range});
		this.noteboard = new NoteboardRenderer({keyprop: 1/4, ctx: this.ctx, noteLocations: this.keyboard.getNoteLocations(), yTime: this.yTime});
	}

	setSections(sections){
		this.sections = sections;
	}

	getExtremes(notes){
		let min = {val: Infinity, note: null}
		let max = {val: -Infinity, note: null}
		notes.forEach(note => {
			if(note["num"] < min.val){
				min.val = note["num"];
				min.note = note;
			}
			if(note["num"] > max.val){
				max.val = note["num"];
				max.note = note;
			}
		})
		return {min: min.note, max: max.note, range: this.getNoteNum(max.note)-this.getNoteNum(min.note)+1}
	}

	// Dom interaction
	generatePopup(hand="left", finger=3){
		this.popup = $(`
			<div id="inputContainer">
				<form id="fingeringForm">
					Finger (1=Thumb, 5=Pinky): <br>
					<input type="text" name="finger" value=${finger} id="fingerInput"> <br><br>
					Hand: <br>
					<input type="radio" name="hand" value="left" id="radioLeft" ${hand === "left" ? "checked" : ""}> Left
					<input type="radio" name="hand" value="right" id="radioRight" ${hand === "right" ? "checked" : ""}> Right <br><br>
					<input type="submit" value="submit">
				</form>
			</div>
		`);
		$('body').append(this.popup)
	}

	removePopup(){
		if(this.popup){
			$(this.popup.children()[0]).unbind();
			this.popup.remove();
			this.popop = false;
		}
	}

	//Aesthetic
	changeTrackColor(trackNum, color){
		this.colors[trackNum%this.colors.length] = color;
	}

	// Events
	createClickEvents(){
		const $canvas = $(this.canvas);
		$canvas.click((e) => {
			this.control.pause();

			const rect = this.canvas.getBoundingClientRect();
			const pos = {x:e.clientX - rect.left, y: e.clientY - rect.top};
			const notes = this.noteLocs.filter(note => pos.x >= note.xMin && pos.x <= note.xMax && pos.y >= note.yMin && pos.y <= note.yMax);
			if(notes.length === 0){
				if(pos.x > 20) return false;
				const sections = this.sectionLocs.filter(sec => pos.x >= sec.xMin && pos.x <= sec.xMax && pos.y >= sec.yMin && pos.y <= sec.yMax);
				if(sections.length === 0){
					const currPos = this.control.getPos();
					const addPos = (1-(pos.y/this.noteboard.boardHeight))*this.noteboard.yTime;
					this.onSectionAdd(currPos+addPos);
					return;
				}
				this.onSectionRm(sections[0]["index"]);
				return;
			}
			const note = notes[0]["note"];
			const last = this.prevNoteMap[note["num"]];
			this.generatePopup(last ? last["hand"] : this.lastHand, last ? last["finger"] : 1);
			$("#fingerInput").select();
			$("#fingeringForm").on("submit", (e) => {
				e.preventDefault();
				const data = $("#fingeringForm :input").serializeArray();
				const hand = data.find(input => input.name === "hand").value;
				const finger = parseInt(data.find(input => input.name === "finger").value);
				if(!["left", "right"].includes(hand) || isNaN(finger) || finger > 5 || finger < 1) return false;
				this.prevNoteMap[note["num"]] = {finger: finger, hand: hand};
				this.lastHand = hand;
				this.onFingerAdd(note["track"], note["index"], hand, finger);
				this.removePopup();
			})
		})
	}

	// Rendering
	render(pos){
		const notes = [];

		var grd = this.ctx.createLinearGradient(0, -this.height, 0, this.height);
		grd.addColorStop(1, "#36454f");
		grd.addColorStop(0, "white");
		this.ctx.fillStyle = grd;
		this.ctx.fillRect(0, 0, this.width, this.height);

		this.notes.forEach(note => {
			if(note["time"] - pos > this.yTime){
				return false;
			}
			if(pos > note["time"]+note["duration"]){
				return false;
			}
			let playing = false;
			if(pos >= note["time"]){
				playing = true;
			}
			notes.push({
				"outTime": note["time"]-pos,
				"duration": note["duration"],
				"num": note["num"],
				"playing": playing,
				"color": note["hand"].length < 1 ? note["color"] : this.handColors[note["hand"]],
				"track": note["track"],
				"index": note["index"],
				"hand": note["hand"],
				"finger": note["finger"]
			})
		})
		const sections = [];
		this.sections.forEach((section, index) => {
			const outTime = section["start"]-pos
			if(outTime > this.yTime){
				return false;
			}
			if(outTime < 0){
				return false;
			}
			sections.push({
				"index": index,
				"outTime": section["start"]-pos
			})
		})
		// ({noteLocations: this.noteLocs, sectionLocations: this.sectionLocs} = this.noteboard.render(notes, sections)); // This errors for some reason. God knows why
		const {noteLocations, sectionLocations} = this.noteboard.render(notes, sections);
		this.noteLocs = noteLocations
		this.sectionLocs = sectionLocations
		this.keyboard.render(notes);
		return {
			noteLocs: this.noteLocs,
			sectionLocs: this.sectionLocs
		}
	}

	// Cleanup
	cleanup(){
		if(this.popup){
			this.removePopup();
		}
	}
}

module.exports = Renderer;